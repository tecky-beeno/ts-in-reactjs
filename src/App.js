import logo from './logo.svg';
import './App.css';
import { name } from './util'

function App() {
  // let name = 'js'
  return (
    <div className="App">
      <header className="App-header">
        {name}
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
